# Update GitLab Runner on runners managers

This runbook describes procedure of upgrading GitLab Runner on our runner managers:

- docker-ci-1.gitlap.com
- docker-ci-2.gitlap.com
- shared-runners-manager-1.gitlab.com
- shared-runners-manager-2.gitlab.com
- omnibus-builder-runners-manager.gitlab.org

## Requirements

To upgrade runners on managers you need to:

- have write access to dev.gitlab.org/cookbooks/chef-repo,
- have write access to chef.gitlab.com,
- have configured knife environment,
- have admin access to nodes (sudo access),
- have admin access to GitLab each instance connected with updated runner (for updating shared runners) **OR**
- have master access to at least one project connected with a specific runner on each GitLab instance
  where runner is used.

## Procedure

> **Notice**: to make update process transparent for users we should update one runner's host
> at a time. For example GitLab CE project on GitLab.com is using four runners: both shared-runners-manager-X
> (as a shared runners), and both docker-ci-X (as specific runners).
>
> If we want to update docker-ci-X we should first update docker-ci-1, and after this update the docker-ci-2.
> It needs to be done like this because of pausing runner - for a time needed to finish running builds the
> runner will not handle new builds.

1. **Shutdown `chef-client` process on managers beeing updated**

    For example, to shutdown chef-client on docker-ci-X.gitlap.com, you can execute:

    ```bash
    $ knife ssh -aipaddress 'role:gitlab-private-runners' -- sudo service chef-client stop
    ```

    To be sure that chef-cilent process is terminated you can execute:

    ```bash
    $ knife ssh -aipaddress 'role:gitlab-private-runners' -- "service chef-client status; ps aux | grep chef"
    ```

1. **Update chef role (or roles)**

    > **Notice:** This needs to be done only onece if you are updating few nodes using the same role.

    While waiting for process to be terminated we can update role (or roles) configuration to set a new
    version of a runner.

    In `chef-repo` directory execute:

    ```bash
    $ rake edit_role[gitlab-private-runners]
    ```

    where `gitlab-private-runners` is a role used by nodes that you are updating. It will be `gitlab-private-runners`
    for docker-ci-X.gitlap.com or `gitlab-shared-runners` for shared-runners-manager-X.gitlab.com.

    For omnibus-builder-runers-manager.gitlab.com you should edit `omnibus-builder-runners-manager` role's secrets:

    ```bash
    $ rake edit_role_secrets[omnibus-builder-runners-manager,_default]
    ```

    In attributes list look for `cookbook-gitlab-runner:gitlab-runner:version` and change it to a version that you want
    to update. It should look like:

    ```json
    "cookbook-gitlab-runner": {
      "chef-vault" : "gitlab-private-runners",
      "gitlab-runner": {
        "repository": "unstable",
        "version": "1.4.0~beta.77.g0ac09d5"
      }
    }
    ```

    If you want to install a Bleeding Edge version of the Runner, you should set the `repository` value to `unstable`.
    If you want to install a Stable version of the Runner, you should set the `repository` value to
    `gitlab-ci-multi-runner` (which is a default if the key doesn't exists in configuration).

1. **Upgrade all GitLab Runners**

    ```
    # For docker-ci-X.gitlap.com
    $ knife ssh -aipaddress 'role:gitlab-private-runners' -- sudo /root/runner_upgrade.sh

    # For shared-runners-manager-X.gitlab.com
    $ knife ssh -aipaddress 'role:gitlab-shared-runners' -- sudo /root/runner_upgrade.sh

    # For Omnibus builders
    $ knife ssh -aipaddress 'role:omnibus-builder-runners-manager' -- sudo /root/runner_upgrade.sh
    ```

1. **Verify the version of GitLab Runner**

    ```
    # For docker-ci-X.gitlap.com
    $ knife ssh -aipaddress 'role:gitlab-private-runners' -- gitlab-runner --version

    # For shared-runners-manager-X.gitlab.com
    $ knife ssh -aipaddress 'role:gitlab-shared-runners' -- gitlab-runner --version

    # For Omnibus builders
    $ knife ssh -aipaddress 'role:omnibus-builder-runners-manager' -- gitlab-runner --version
    ```

1. **Update GitLab.com's configuration description**

    If you are updating shared runners used by GitLab.com, please create a merge request in
    https://gitlab.com/gitlab-com/www-gitlab-com project to update Runner's version and/or any other changed
    configuration values which are specified at
    https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/gitlab-com/settings/index.html.md#shared-runners.
